//
//  RLCategoryContentsModel.m
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import "RLCategoryItemModel.h"

@implementation RLCategoryItemModel

-(RLCategoryItemModel *)initWithItemName:(NSString *)itemName image:(UIImage *)itemImage andRating:(float)itemRating {
    self.itemName = itemName;
    self.itemImage = itemImage;
    self.itemRating = itemRating;
    return self;
}

@end
