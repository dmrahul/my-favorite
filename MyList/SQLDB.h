//
//  SQLDB.h
//  MyList
//
//  Created by Rahul Likhyani on 29/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SQLDB : NSObject

@property(strong, nonatomic) NSString *databasePath;
@property(nonatomic) sqlite3 *categoryDB;

+(void)insertRowWithName:(NSString *)name andImage:(NSString *)imageURL;
+(void)insertRowWithTableName:(NSString *)tableName itemName:(NSString *)itemName image:(NSString *)itemImageURL andRating:(float)rating;

+(BOOL)createDatabase;

+(NSMutableArray *)retrieveAllRows;
+(NSMutableArray *)retrieveAllRowsFromTable:(NSString *)tableName;

+(BOOL)deleteRowWithName:(NSString *)name;
+(BOOL)deleteRowWithTableName:(NSString *)tableName andName:(NSString *)name;

+(BOOL)createTableWithName:name;
+(BOOL)deleteTableWithName:name;

@end
