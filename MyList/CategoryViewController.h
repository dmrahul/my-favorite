//
//  CategoryViewController.h
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UITableViewController

@property(strong, nonatomic) NSMutableArray *categoryArray;

@end
