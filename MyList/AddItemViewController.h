//
//  AddItemViewController.h
//  MyList
//
//  Created by Rahul Likhyani on 30/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@interface AddItemViewController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate,RateViewDelegate>

@property (weak, nonatomic) IBOutlet RateView *rateView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property( strong, nonatomic) UIImagePickerController *picker;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
- (IBAction)addImageButton:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
@end
