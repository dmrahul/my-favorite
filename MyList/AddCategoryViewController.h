//
//  CategoryItemsViewController.h
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCategoryViewController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property( strong, nonatomic) UIImagePickerController *picker;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
- (IBAction)addImageButton:(id)sender;
- (IBAction)hideKeyboard:(id)sender;

@end
