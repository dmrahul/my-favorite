//
//  SQLDB.m
//  MyList
//
//  Created by Rahul Likhyani on 29/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import "SQLDB.h"
#import "RLCategoryModel.h"
#import "RLCategoryItemModel.h"

@implementation SQLDB
NSString *databasePath;
sqlite3 *categoryDB;

+(void)insertRowWithName:(NSString *)name andImage:(NSString *)imageURL {
    
    sqlite3_stmt *statement;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK) {
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO CATEGORY(name, image) VALUES(\"%@\", \"%@\")",name, imageURL];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(categoryDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"Row Inserted Successfully");
            
        }
        else {
            NSLog(@"Unable to insert row");
        }
        sqlite3_finalize(statement);
        sqlite3_close(categoryDB);
        
    }
    
}

+(void)insertRowWithTableName:(NSString *)tableName itemName:(NSString *)itemName image:(NSString *)itemImageURL andRating:(float)rating {
    sqlite3_stmt *statement;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK) {
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO %@(item_name, item_image, item_rating) VALUES('%@', '%@', '%f')",tableName, itemName, itemImageURL, rating];
        NSLog(@"%@", insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(categoryDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"Row Inserted Successfully");
            
        }
        else {
            NSLog(@"Unable to insert row");
        }
        sqlite3_finalize(statement);
        sqlite3_close(categoryDB);
        
    }

}


                                
+(BOOL)createDatabase {
   // SQLDB *sql = [[SQLDB alloc]init];
    NSString *docsDir;
    NSArray *dirPaths;
    
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    databasePath = [[NSString alloc]initWithString:[docsDir stringByAppendingPathComponent:@"category.db"]];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if([fileMgr fileExistsAtPath:databasePath] == NO) {
        const char *database_Path = [databasePath UTF8String];
        if (sqlite3_open(database_Path, &categoryDB) == SQLITE_OK) {
            
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS CATEGORY(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, IMAGE TEXT)";
            if (sqlite3_exec(categoryDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
            }
            else {
                NSLog(@"Successfully created database and table");
                //sql.categoryDB = categoryDB;
                //sql.databasePath = dbPath;
            }
            sqlite3_close(categoryDB);
        }else {
            
            NSLog(@"Failed to open/create");
        }
    }
    if([fileMgr fileExistsAtPath:databasePath] == YES)
        return YES;
    else
        return NO;
        
    
                                    
}

+(NSMutableArray *)retrieveAllRows {
    NSMutableArray *catArray = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK) {
        NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM category"];
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(categoryDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW) {
                //NSString  *idx = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_int(statement,0)];
                NSString *nameField = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement,1)];
                NSString *imageURL = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                //NSString *objectString = [NSString stringWithFormat:@"*row%@",idx];
                NSURL *url = [NSURL fileURLWithPath:imageURL];
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                UIImage *image = [UIImage imageWithData:imageData];
                RLCategoryModel *row = [[RLCategoryModel alloc]initWithName:nameField andImage:image];
                [catArray addObject:row];
            }
            sqlite3_finalize(statement);
            sqlite3_close(categoryDB);
            return catArray;
        }
        
    }
    return nil;
}
+(NSMutableArray *)retrieveAllRowsFromTable:(NSString *)tableName {
    NSMutableArray *itemArray = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK) {
        NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM %@",tableName];
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(categoryDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW) {
                //NSString  *idx = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_int(statement,0)];
                NSString *nameField = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement,1)];
                NSString *imageURL = [[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                float rating = sqlite3_column_double(statement, 3);
                //NSString *objectString = [NSString stringWithFormat:@"*row%@",idx];
                NSURL *url = [NSURL fileURLWithPath:imageURL];
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                UIImage *image = [UIImage imageWithData:imageData];
                RLCategoryItemModel *row = [[RLCategoryItemModel alloc]initWithItemName:nameField image:image andRating:rating];
                [itemArray addObject:row];
            }
            sqlite3_finalize(statement);
            sqlite3_close(categoryDB);
            return itemArray;
        }
        
    }
    return nil;
}


+(BOOL)deleteRowWithName:(NSString *)name
{
            NSString *docsDir;
            NSArray *dirPaths;
            NSError *error;
            
            
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = dirPaths[0];
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            NSString *imageFilePath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",name]];
            //NSLog(imageFilePath);
            if([fileMgr removeItemAtPath:imageFilePath error:&error])
            {
                sqlite3_stmt *statement;

                const char *dbpath = [databasePath UTF8String];
                if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK)
                {
                    NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM category WHERE name = '%@'",name];
//                    NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM category"];

                    const char *delete_stmt = [deleteSQL UTF8String];
                    sqlite3_prepare_v2(categoryDB, delete_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        sqlite3_finalize(statement);
                        sqlite3_close(categoryDB);
                        return YES;
                    }
                }
            }
            else
            {
                        NSLog(@"Unable to delete row");
                        
                        return NO;
            }
            
    return NO;
    
}

+(BOOL)deleteRowWithTableName:(NSString *)tableName andName:(NSString *)name
{
    NSString *docsDir;
    NSArray *dirPaths;
    NSError *error;
    
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *imageFilePath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_item.png",name]];
    //NSLog(imageFilePath);
    if([fileMgr removeItemAtPath:imageFilePath error:&error])
    {
        sqlite3_stmt *statement;
        
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK)
        {
            NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM %@ WHERE item_name = '%@'",tableName,name];
            //                    NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM category"];
            
            const char *delete_stmt = [deleteSQL UTF8String];
            NSLog(@"%@",deleteSQL);
            sqlite3_prepare_v2(categoryDB, delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
                sqlite3_close(categoryDB);
                return YES;
            }
        }
    }
    else
    {
        NSLog(@"Unable to delete row");
        
        return NO;
    }
    
    return NO;
    
}

+(BOOL)createTableWithName:name {
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK) {
        sqlite3_stmt *statement;

        NSString *createTableSQL = [NSString stringWithFormat: @"CREATE TABLE IF NOT EXISTS %@(ITEM_ID INTEGER PRIMARY KEY AUTOINCREMENT, ITEM_NAME TEXT, ITEM_IMAGE TEXT, ITEM_RATING FLOAT)",name];
        const char *createTable_stmt = [createTableSQL UTF8String];
        sqlite3_prepare_v2(categoryDB, createTable_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"%@ table successfully created",name);
            sqlite3_finalize(statement);
            return YES;
            
        }
        else {
            NSLog(@"Unable to create %@ table",name);
            //sqlite3_finalize(statement);
        }
        sqlite3_close(categoryDB);

        
    }
    //sqlite3_finalize(statement);
    //sqlite3_close(categoryDB);
    return NO;
}


+(BOOL)deleteTableWithName:name {
    
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &categoryDB) == SQLITE_OK) {
        sqlite3_stmt *statement;
        NSString *deleteTableSQL = [NSString stringWithFormat: @"DROP TABLE %@",name];
        const char *deleteTable_stmt = [deleteTableSQL UTF8String];
        sqlite3_prepare_v2(categoryDB, deleteTable_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"%@ table successfully deleted",name);
            sqlite3_finalize(statement);
            sqlite3_close(categoryDB);
            return YES;
            
        }
        else {
            NSLog(@"Unable to delete %@ table",name);
            //sqlite3_finalize(statement);
            sqlite3_close(categoryDB);
        }
        
        
    }
    //sqlite3_finalize(statement);
    //sqlite3_close(categoryDB);
    return NO;
}

@end
