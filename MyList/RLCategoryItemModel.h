//
//  RLCategoryContentsModel.h
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLCategoryModel.h"

@interface RLCategoryItemModel : NSObject

@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) UIImage *itemImage;
@property (nonatomic) float itemRating;

-(RLCategoryItemModel *)initWithItemName:(NSString *)itemName image:(UIImage *)itemImage andRating:(float)itemRating;
@end
