//
//  ItemListViewController.h
//  MyList
//
//  Created by Rahul Likhyani on 30/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemListViewController : UITableViewController

@property(strong, nonatomic) NSMutableArray *itemArray;

@end
