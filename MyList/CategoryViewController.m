//
//  CategoryViewController.m
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import "CategoryViewController.h"
#import "RLCategoryModel.h"
#import "AddCategoryViewController.h"
#import "SQLDB.h"
#import "ItemListViewController.h"

@interface CategoryViewController ()

@property(strong, nonatomic) NSString *tableName;
@end

@implementation CategoryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.categoryArray = [[NSMutableArray alloc]init];
    if([SQLDB createDatabase]) {
        self.categoryArray = [SQLDB retrieveAllRows];
        NSLog(@"%@",_categoryArray);
        
        
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"%i",self.categoryArray.count);
    return self.categoryArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    RLCategoryModel *categoryObject = self.categoryArray[indexPath.row];
    NSString *cellName = categoryObject.categoryName;
    UIImage *cellImage = categoryObject.categoryImage;
    
    cell.textLabel.text = cellName;
    cell.imageView.image = cellImage;
    
    return cell;
}


-(IBAction)returnedFromAddCategory:(UIStoryboardSegue *)segue {
    AddCategoryViewController *svc = segue.sourceViewController;
//    RLCategoryModel *newCategory = [[RLCategoryModel alloc]initWithName:svc.nameTextField.text andImage:svc.imageView.image];
//    [self.categoryArray addObject:newCategory];
//    [self.tableView reloadData];
    NSString *name = svc.nameTextField.text;
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(svc.imageView.image)];
    NSString *imagePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    imagePath = [imagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",name]];
    [imageData writeToFile:imagePath atomically:YES];
    
    [SQLDB insertRowWithName:name andImage:imagePath];
    if([SQLDB createTableWithName:name]) {
        RLCategoryModel *newRow = [[RLCategoryModel alloc]initWithName:name andImage:svc.imageView.image];
        [self.categoryArray addObject:newRow];
        [self.tableView reloadData];
    }
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        RLCategoryModel *row = self.categoryArray[indexPath.row];
        NSString *categoryName = row.categoryName;
        if([SQLDB deleteTableWithName:categoryName]) {
            
        
            if([SQLDB deleteRowWithName:categoryName]) {
                [self.categoryArray removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    RLCategoryModel *row = self.categoryArray[indexPath.row];
//    self.tableName = row.categoryName;
//    NSLog(@"%@",self.tableName);
//}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"SegueToItemList"]) {
        UITableViewCell *cell = (UITableViewCell *)sender;
        self.tableName = cell.textLabel.text;
        ItemListViewController *dvc = segue.destinationViewController;
        dvc.navigationItem.title = self.tableName;
        
        
        
    }
    
}


@end
