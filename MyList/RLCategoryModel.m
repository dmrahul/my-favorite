//
//  RLCategoryModel.m
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import "RLCategoryModel.h"

@implementation RLCategoryModel

-(RLCategoryModel *)initWithName:(NSString *)name andImage:(UIImage *)image {
    self.categoryName = name;
    self.categoryImage = image;
    return self;
    
}
@end
