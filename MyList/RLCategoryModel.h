//
//  RLCategoryModel.h
//  MyList
//
//  Created by Rahul Likhyani on 28/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RLCategoryModel : NSObject

@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) UIImage *categoryImage;

-(RLCategoryModel *)initWithName:(NSString *)name andImage:(UIImage *)image;

@end
