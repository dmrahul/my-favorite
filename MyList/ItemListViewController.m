//
//  ItemListViewController.m
//  MyList
//
//  Created by Rahul Likhyani on 30/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import "ItemListViewController.h"
#import "AddItemViewController.h"
#import "SQLDB.h"
#import "RLCategoryItemModel.h"
#import "ItemViewController.h"

@interface ItemListViewController ()

@end

@implementation ItemListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.itemArray = [SQLDB retrieveAllRowsFromTable:self.navigationItem.title];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.itemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"itemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    RLCategoryItemModel *row = self.itemArray[indexPath.row];
    cell.textLabel.text = row.itemName;
    cell.imageView.image = row.itemImage;
    
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the row from the data source
        RLCategoryItemModel *row = self.itemArray[indexPath.row];
        NSString *itemName = row.itemName;
        
            
            
            if([SQLDB deleteRowWithTableName:self.navigationItem.title andName:itemName]) {
                [self.itemArray removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        
        
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

-(IBAction)returnedFromAddItem:(UIStoryboardSegue *)segue {
    AddItemViewController *svc = segue.sourceViewController;
    NSString *name = svc.nameTextField.text;
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(svc.imageView.image)];
    NSString *imagePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    imagePath = [imagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_item.png",name]];
    [imageData writeToFile:imagePath atomically:YES];
    NSString *tableName = self.navigationItem.title;
    float rating = svc.rateView.rating;
    [SQLDB insertRowWithTableName:tableName itemName:name image:imagePath andRating:rating];
    
    RLCategoryItemModel *newRow = [[RLCategoryItemModel alloc]initWithItemName:name image:svc.imageView.image andRating:rating];
        [self.itemArray addObject:newRow];
        [self.tableView reloadData];
    

    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showItemSegue"]) {
        UITableViewCell *cell = (UITableViewCell *)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        RLCategoryItemModel *item = self.itemArray[indexPath.row];
        NSString *name = item.itemName;
        UIImage *image = item.itemImage;
        float rating = item.itemRating;
        NSLog(@"%@  %f",name,rating);
        ItemViewController *itemVC = segue.destinationViewController;
        //itemVC.navigationItem.title = name;
        itemVC.name = name;
        itemVC.image = image;
        itemVC.rating = rating;
    }
    
}


@end
