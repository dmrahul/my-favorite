//
//  ItemViewController.h
//  MyList
//
//  Created by Rahul Likhyani on 31/10/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
@interface ItemViewController : UITableViewController <RateViewDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet RateView *rateView;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic) float rating;
- (IBAction)hideKeyboard:(id)sender;
- (IBAction)shareButtonTapped:(id)sender;

@end
