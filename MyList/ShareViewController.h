//
//  ShareViewController.h
//  MyList
//
//  Created by Rahul Likhyani on 14/11/13.
//  Copyright (c) 2013 Rahul Likhyani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareViewController : UITableViewController
- (IBAction)closeTapped:(id)sender;

@end
